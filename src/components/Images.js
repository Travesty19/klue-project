import React, { PureComponent } from 'react';
import '../styles/Images.scss';  

class Images extends PureComponent {
  render() {
    const item = this.props.item
    return (
      <div className="Container">
        <img className="Image" src={item[0].urls.full} />
        <img className="Image" src={item[1].urls.full} />
      </div>
    );
  }
}

export default Images;