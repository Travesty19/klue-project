import React, { Component } from 'react';
import '../styles/InputBox.scss';  

class InputBox extends Component { 
  render() {
    return(
      <div className='Container'>
        <input type='text' id='input' value={this.props.input} onChange={this.props.onChange}></input>
        <button className='Button' type='submit' id='submit' onClick={this.props.onClick} >Search</button>
        <div className="Subtext" >{this.props.value}</div>
      </div> 
    );
  }
} 

export default InputBox;