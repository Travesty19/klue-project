import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Images from './components/Images';
import InputBox from './components/InputBox';
import { shallow } from 'enzyme';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

//Images Test
it('renders without crashing', () => {
  const item = [
    {
      urls: {
        full: 'https://fakeimg.pl/300/'
      }
    },
    {
      urls: {
        full: 'https://fakeimg.pl/250x100/'
      }
    }
  ]
  shallow(<Images item={item} />);
});

//InputBox Test
it('renders without crashing', () => {
  shallow(        
    <InputBox 
      input={'input_test'} 
      value={'value_test'}
      onChange={console.log('Test')} 
      onClick={console.log('Test')}
    />
  );
});

