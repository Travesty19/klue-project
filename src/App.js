import React, { Component } from 'react'; 
import Images from './components/Images'
import InputBox from './components/InputBox'
import './styles/App.scss'; 

const AppId = '1424074b20f17701ec8c0601fd15ca686c70e2cb0e645f8137533d8063e664bc'

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      input: ' ',
      value: '',
    }
    this.handleChange = this.handleChange.bind(this); 
    this.handleClick = this.handleClick.bind(this); 
  } 

  componentDidMount() {
    document.title = 'Klue Project'
    fetch('https://api.unsplash.com/photos/?page=1&per_page=20&client_id=' + AppId )
      .then(res => res.json())
      .then(data => {
        //Sort response into columns
        var images = []
        data.forEach((item, index) => {
          if (index % 2) {
            images.push([data[index - 1], data[index]]);
          }
        });
        this.setState({ items: images });
      })
      .catch(err => {
        console.log('Error happened during fetching!', err);
      });
  }
  
  handleClick = () => {
    const input = this.state.input
    fetch('https://api.unsplash.com/search/photos/?page=1&per_page=20&query=' + input + '&client_id=' + AppId)
    .then(res => res.json())
    .then(data => {
      //Sort response into columns
      var images = []
      data.results.forEach((item, index) => {
        if (index % 2) {
          images.push([data.results[index - 1], data.results[index]]);
        }
      });
      this.setState({ items: images}); 
    })
    .catch(err => {
      console.log('Error happened during fetching!', err);
    });
    this.setState({
      value: "Your search term is: " + this.state.input, 
      input: ''
    })
  }  

  handleChange = (event) => {
    this.setState({
      input: event.target.value,
    })
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <h1 className="Header-text">Find An Image</h1>
          <InputBox 
            input={this.state.input} 
            value={this.state.value}
            onChange={this.handleChange} 
            onClick={this.handleClick}/>
        </div> 
        <div className="Images-container">  
          {this.state.items.map(item => <Images key={item[0].id} item={item}/>)}
        </div>
      </div> 
    );
  }
}

export default App;